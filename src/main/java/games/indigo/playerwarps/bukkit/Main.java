package games.indigo.playerwarps.bukkit;

import games.indigo.databaseconnector.DatabaseConnector;
import games.indigo.playerwarps.bukkit.listeners.*;
import games.indigo.playerwarps.bukkit.commands.PlayerWarpsCommand;
import games.indigo.playerwarps.bukkit.utils.EconomyHook;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;

public class Main extends JavaPlugin {

    // TODO: Put a limit on how many warps you can create (related to ranks?)

    private static Main instance;

    private static DatabaseConnector database;
    private static WarpManager warpManager;

    public void onEnable() {
        instance = this;
        database = Bukkit.getServicesManager().getRegistration(DatabaseConnector.class).getProvider();
        warpManager = new WarpManager();

        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeCord", new PluginMessages());

        // Commands
        getCommand("pwarp").setExecutor(new PlayerWarpsCommand());

        // Listeners
        registerListeners(
                new PlayerWarpCreateListener(),
                new PlayerWarpDeleteListener(),
                new InventoryClickListener(),
                new PlayerChatListener()
        );

        EconomyHook.setupEconomy();

        // Make sure pwarps database exists
        getWarpManager().createDatabase();
        // Load pwarps into memory
        getWarpManager().loadWarps();
    }

    private void registerListeners(Listener... listeners) {
        Arrays.asList(listeners).forEach(listener -> getServer().getPluginManager().registerEvents(listener, this));
    }

    public static Main getInstance() { return instance; }

    public WarpManager getWarpManager() { return warpManager; }

    DatabaseConnector getDatabase() { return database; }
}

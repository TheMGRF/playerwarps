package games.indigo.playerwarps.bukkit.listeners;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import games.indigo.playerwarps.bukkit.Main;
import games.indigo.playerwarps.core.PlayerWarp;
import games.indigo.playerwarps.bukkit.utils.Text;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.util.UUID;

public class PluginMessages implements PluginMessageListener {

    @Override
    public synchronized void onPluginMessageReceived(String channel, Player player, byte[] message) {
        if (!channel.equals("BungeeCord")) return;

        ByteArrayDataInput in = ByteStreams.newDataInput(message);
        String subChannel = in.readUTF();

        if (subChannel.contains("PlayerWarps")) {
            Bukkit.getScheduler().runTaskLater(Main.getInstance(), () -> {
                String uuid = in.readUTF();
                Player p = Bukkit.getPlayer(UUID.fromString(uuid));
                if (p != null) {
                    PlayerWarp playerWarp = new PlayerWarp().fromString(in.readUTF());

                    p.teleport(Main.getInstance().getWarpManager().asBukkitLocation(playerWarp.getLocation()));
                    p.playSound(p.getLocation(), Sound.ENTITY_ILLUSIONER_MIRROR_MOVE, 1, 0.1f);
                    String name = ChatColor.translateAlternateColorCodes('&', playerWarp.getName());
                    p.sendMessage(Text.colour("&dTeleported to " + name + " &downed by &e" + Bukkit.getOfflinePlayer(UUID.fromString(playerWarp.getUUID())).getName()));
                }
            }, 10);
        } else if (subChannel.equals("PlayerWarpsRefresh")) {
            Bukkit.getScheduler().runTaskLater(Main.getInstance(), () -> Main.getInstance().getWarpManager().loadWarps(), 5);
        }
    }
}

/*
if (subChannel.equals("PlayerWarps")) {
            Bukkit.getScheduler().runTaskLater(Main.getInstance(), () -> {
                Bukkit.getOnlinePlayers().forEach(loop -> {
                    if (loop.getUniqueId().toString().equals(in.readUTF())) {
                        PlayerWarp playerWarp = new PlayerWarp().fromString(in.readUTF());

                        loop.teleport(Main.getInstance().getWarpManager().asBukkitLocation(playerWarp.getLocation()));
                        loop.playSound(loop.getLocation(), Sound.ENTITY_ILLUSIONER_MIRROR_MOVE, 1, 0.1f);
                        String name = ChatColor.translateAlternateColorCodes('&', playerWarp.getName());
                        loop.sendMessage(Text.colour("&dTeleported to " + name + " &downed by &e" + Bukkit.getOfflinePlayer(UUID.fromString(playerWarp.getUUID())).getName()));
                        return;
                    }
                });
            }, 21);
 */
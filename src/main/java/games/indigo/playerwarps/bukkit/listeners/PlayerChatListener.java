package games.indigo.playerwarps.bukkit.listeners;

import games.indigo.playerwarps.bukkit.Main;
import games.indigo.playerwarps.core.PlayerWarp;
import games.indigo.playerwarps.bukkit.events.PlayerWarpCreateEvent;
import games.indigo.playerwarps.bukkit.utils.EconomyHook;
import games.indigo.playerwarps.bukkit.utils.Text;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class PlayerChatListener implements Listener {

    private Main main = Main.getInstance();

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        Player player = e.getPlayer();

        String pWarpName, pWarpDesc;

        if (player.hasMetadata("pWarpsCurrent")) {
            if (e.getMessage().equalsIgnoreCase("cancel")) {
                player.removeMetadata("pWarpsName", main);
                player.removeMetadata("pWarpsDesc", main);
                player.removeMetadata("pWarpsCurrent", main);
                player.sendMessage(Text.colour(Text.PREFIX + "&4Cancelled the creation of your warp."));
            } else {
                PlayerWarp playerWarp = (PlayerWarp) player.getMetadata("pWarpsCurrent").get(0).value();

                if (player.hasMetadata("pWarpsName")) {

                    if (e.getMessage().contains("_")) {
                        e.setCancelled(true);
                        player.sendMessage(Text.colour(Text.PREFIX + "&cYour warp name cannot contain underscores! Please use a different name for your warp:"));
                        return;
                    }

                    for (PlayerWarp temp : main.getWarpManager().getPlayerWarps()) {
                        if (temp.getUUID().equals(player.getUniqueId().toString())) {
                            if (temp.getName().equals(Text.colour("&f" + e.getMessage()))) {
                                e.setCancelled(true);
                                player.sendMessage(Text.colour(Text.PREFIX + "&cYou already have a warp with that name! Please use a different name for your warp:"));
                                return;
                            }
                        }
                    }

                    player.removeMetadata("pWarpsName", main);

                    pWarpName = "&f" + e.getMessage();
                    player.sendMessage(Text.colour(Text.PREFIX + "&aPlayer warp name set to: " + pWarpName));
                    playerWarp.setName(pWarpName);
                    player.setMetadata("pWarpsCurrent", new FixedMetadataValue(main, playerWarp));

                    player.setMetadata("pWarpsDesc", new FixedMetadataValue(main, true));
                    player.sendMessage(Text.colour(Text.PREFIX + "&7Please enter a description for your warp:"));
                } else if (player.hasMetadata("pWarpsDesc")) {
                    player.removeMetadata("pWarpsDesc", main);
                    player.removeMetadata("pWarpsCurrent", main);
                    pWarpDesc = "&7" + e.getMessage();
                    player.sendMessage(Text.colour(Text.PREFIX + "&aPlayer warp description set to: " + pWarpDesc));
                    playerWarp.setDescription(pWarpDesc);

                    if (player.hasMetadata("pWarpsEditing")) {
                        player.removeMetadata("editingPWarp", main);
                        player.sendMessage(Text.colour(Text.PREFIX + "&aWarp successfully updated!"));
                    } else {
                        int cost = EconomyHook.getCost();
                        if (EconomyHook.getEconomy().has(player, cost)) {
                            EconomyHook.getEconomy().withdrawPlayer(player, cost);

                            Bukkit.getConsoleSender().sendMessage(Text.colour("&7[PlayerWarps] &e" + player.getName() + " &7just created a new warp: " + playerWarp.getName()));

                            player.sendMessage("");
                            player.sendMessage(Text.colour("&b&lPlayer Warp Created!"));
                            player.sendMessage(Text.colour(" &7Name: " + playerWarp.getName()));
                            player.sendMessage(Text.colour(" &7Description: " + playerWarp.getDescription()));
                            player.sendMessage(Text.colour(" &7Location: &e" + playerWarp.getLocation().getX() + "," + playerWarp.getLocation().getY() + "," + playerWarp.getLocation().getZ()));
                            player.sendMessage(Text.colour(" &7Item: &f" + playerWarp.getMaterial()));
                            player.sendMessage("");

                            Bukkit.getScheduler().runTask(main, () -> {
                                Bukkit.getServer().getPluginManager().callEvent(new PlayerWarpCreateEvent(playerWarp, player));
                            });
                        } else {
                            player.sendMessage(Text.colour(Text.PREFIX + "&cYou do not have enough money for a warp. &a" + EconomyHook.getEconomy().format(EconomyHook.getEconomy().getBalance(player)) + "&7/&c$100,000"));
                        }
                    }
                }
            }
            e.setCancelled(true);
        }
    }
}

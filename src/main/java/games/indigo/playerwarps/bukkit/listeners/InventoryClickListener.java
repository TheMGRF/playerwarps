package games.indigo.playerwarps.bukkit.listeners;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import games.indigo.playerwarps.bukkit.Main;
import games.indigo.playerwarps.bukkit.events.PlayerWarpDeleteEvent;
import games.indigo.playerwarps.core.PlayerWarp;
import games.indigo.playerwarps.bukkit.guis.ConfirmDeleteGUI;
import games.indigo.playerwarps.bukkit.guis.OwnWarpsGUI;
import games.indigo.playerwarps.bukkit.guis.PlayerWarpsGUI;
import games.indigo.playerwarps.bukkit.utils.EconomyHook;
import games.indigo.playerwarps.bukkit.utils.Text;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.ArrayList;
import java.util.UUID;

public class InventoryClickListener implements Listener {

    private Main main = Main.getInstance();

    @EventHandler
    public void onInvClick(InventoryClickEvent e) {
        Player player = (Player) e.getWhoClicked();

        if (e.getView().getTitle().equals(Text.colour("&b&lPlayer Warps &8(" + player.getName() + ")"))) {
            e.setCancelled(true);
            if (e.getClickedInventory() != null && e.getClickedInventory() != e.getView().getBottomInventory()) {
                if (e.getCurrentItem() != null && e.getCurrentItem().getType() != Material.AIR) {
                    player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);

                    if (e.getSlot() < 45) {

                        // Create 2nd array to store just the players warps
                        ArrayList<PlayerWarp> ownWarps = new ArrayList<>();
                        for (PlayerWarp playerWarp : main.getWarpManager().getPlayerWarps()) {
                            if (player.getUniqueId().toString().equals(playerWarp.getUUID())) {
                                ownWarps.add(playerWarp);
                            }
                        }

                        PlayerWarp playerWarp = ownWarps.get(e.getSlot());
                        if (e.getClick() == ClickType.LEFT) {
                            player.teleport(main.getWarpManager().asBukkitLocation(playerWarp.getLocation()));
                            player.playSound(player.getLocation(), Sound.ENTITY_ILLUSIONER_MIRROR_MOVE, 1, 0.1f);
                            player.sendMessage(Text.colour("&dTeleported to your warp: " + playerWarp.getName()));
                        } else if (e.getClick() == ClickType.RIGHT) {
                            player.setMetadata("deletePWarp", new FixedMetadataValue(main, playerWarp));
                            ConfirmDeleteGUI.open(player);
                            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1, 1);
                        } else if (e.getClick() == ClickType.MIDDLE) {
                            Material material = player.getInventory().getItemInMainHand().getType();
                            if (material != Material.AIR) {
                                playerWarp.setMaterial(material.name());
                                player.sendMessage(Text.colour(Text.PREFIX + "&aWarp icon set to &f" + material));
                                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1, 1);
                                OwnWarpsGUI.open(player);
                            } else {
                                player.sendMessage(Text.colour(Text.PREFIX + "&cPlease hold the item you wish to use as an icon for your warp then try again."));
                                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1, 1);
                            }
                        } else if (e.getClick() == ClickType.SHIFT_LEFT) {
                            Bukkit.getScheduler().runTaskLater(main, new Runnable() {
                                @Override
                                public void run() {
                                    player.closeInventory();
                                }
                            }, 1);

                            player.setMetadata("pWarpsName", new FixedMetadataValue(main, true));
                            player.setMetadata("pWarpsCurrent", new FixedMetadataValue(main, playerWarp));
                            player.setMetadata("pWarpsEditing", new FixedMetadataValue(main, true));
                            player.sendMessage(Text.colour(Text.PREFIX + "&7Please enter a name for your warp &c(to exit this type \"cancel\")&7:"));
                        } else if (e.getClick() == ClickType.SHIFT_RIGHT) {
                            Bukkit.getScheduler().runTaskLater(main, new Runnable() {
                                @Override
                                public void run() {
                                    player.closeInventory();
                                }
                            }, 1);

                            playerWarp.setLocation(main.getWarpManager().asPWLocation(player.getLocation()));
                            player.sendMessage(Text.colour(Text.PREFIX + "&aSuccessfully updated player warp location to your position!"));
                        }
                    } else {
                        ItemStack item = e.getCurrentItem();
                        if (item.getItemMeta().getDisplayName().equals(Text.colour("&6View All Warps"))) {
                            PlayerWarpsGUI.open((Player) e.getWhoClicked(), 1);
                        } else if (item.getItemMeta().getDisplayName().equals(Text.colour("&dCreate Warp"))) {
                            if (EconomyHook.getEconomy().has(player, EconomyHook.getCost())) {
                                Bukkit.getScheduler().runTaskLater(main, (Runnable) player::closeInventory, 1);
                                player.performCommand("pwarps create");
                                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1, 1);
                            } else {
                                player.performCommand("pwarps create");
                                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1, 1);
                            }
                        }
                    }
                }
            }
        } else if (e.getView().getTitle().contains(Text.colour("&b&lPlayer Warps"))) {
            e.setCancelled(true);

            if (e.getClickedInventory() != null && e.getClickedInventory() != e.getView().getBottomInventory()) {
                if (e.getSlot() < 45 && e.getCurrentItem() != null) {
                    if (e.getCurrentItem().getType() != Material.AIR) {
                        player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);

                        int page = player.getMetadata("pwarpsPage").get(0).asInt();
                        PlayerWarp playerWarp = main.getWarpManager().getPlayerWarps().get(e.getSlot() + (45 * (page - 1)));

                        // Check to see if warp is on current server - if not send bungee message
                        if (playerWarp.getServer().equalsIgnoreCase(player.getWorld().getName())) {
                            player.teleport(main.getWarpManager().asBukkitLocation(playerWarp.getLocation()));
                            player.playSound(player.getLocation(), Sound.ENTITY_ILLUSIONER_MIRROR_MOVE, 1, 0.1f);
                            String name = ChatColor.translateAlternateColorCodes('&', playerWarp.getName());
                            player.sendMessage(Text.colour("&dTeleported to " + name + " &downed by &e" + Bukkit.getOfflinePlayer(UUID.fromString(playerWarp.getUUID())).getName()));
                        } else {
                            player.closeInventory();
                            ByteArrayDataOutput out = ByteStreams.newDataOutput();
                            out.writeUTF("PlayerWarps");
                            out.writeUTF(player.getUniqueId().toString());
                            out.writeUTF(playerWarp.toString());

                            player.sendPluginMessage(Main.getInstance(), "BungeeCord", out.toByteArray());
                        }
                    }
                } else if (e.getCurrentItem() != null) {
                    if (e.getCurrentItem().getType() != Material.AIR) {
                        player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);

                        ItemStack item = e.getCurrentItem();
                        int page = player.getMetadata("pwarpsPage").get(0).asInt();
                        if (item.getItemMeta().getDisplayName().equals(Text.colour("&6View Own Warps"))) {
                            OwnWarpsGUI.open(player);
                        } else if (item.getItemMeta().getDisplayName().equals(Text.colour("&bPrevious Page"))) {
                            if (page > 1) {
                                PlayerWarpsGUI.open(player, page - 1);
                                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1, 1);
                            } else {
                                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1, 1);
                            }
                        } else if (item.getItemMeta().getDisplayName().equals(Text.colour("&eRefresh"))) {
                            PlayerWarpsGUI.open(player, 1);
                            player.playSound(player.getLocation(), Sound.BLOCK_PISTON_EXTEND, 1, 2);
                            Bukkit.getScheduler().runTaskLater(main, new Runnable() {
                                @Override
                                public void run() {
                                    player.playSound(player.getLocation(), Sound.BLOCK_PISTON_CONTRACT, 1, 2);
                                }
                            }, 3);
                        } else if (item.getItemMeta().getDisplayName().equals(Text.colour("&bNext Page"))) {
                            if (e.getInventory().getItem(44) != null) {
                                PlayerWarpsGUI.open(player, page + 1);
                                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1, 1);
                            } else {
                                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1, 1);
                            }
                        } else if (item.getItemMeta().getDisplayName().equals(Text.colour("&cExit"))) {
                            player.closeInventory();
                        }
                    }
                }
            }
        } else if (e.getView().getTitle().equals(Text.colour("&4Delete Warp &8| &aAre you sure?"))) {
            e.setCancelled(true);

            if (e.getClickedInventory() != null && e.getView().getTitle().equals(Text.colour("&4Delete Warp &8| &aAre you sure?"))) {
                if (e.getCurrentItem() != null && e.getCurrentItem().getType() != Material.AIR) {
                    ItemStack item = e.getCurrentItem();
                    if (item.getItemMeta().getDisplayName().equals(Text.colour("&a&lCONFIRM"))) {
                        PlayerWarp playerWarp = (PlayerWarp) player.getMetadata("deletePWarp").get(0).value();
                        Main.getInstance().getWarpManager().removePlayerWarp(playerWarp);
                        player.removeMetadata("deletePWarp", main);
                        player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1, 1);
                        PlayerWarpsGUI.open(player, 1);

                        Bukkit.getScheduler().runTask(main, () -> {
                            Bukkit.getServer().getPluginManager().callEvent(new PlayerWarpDeleteEvent(playerWarp, player));
                        });
                    } else if (item.getItemMeta().getDisplayName().equals(Text.colour("&c&lCANCEL"))) {
                        OwnWarpsGUI.open(player);
                        player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                    }
                }
            }
        }
    }

}

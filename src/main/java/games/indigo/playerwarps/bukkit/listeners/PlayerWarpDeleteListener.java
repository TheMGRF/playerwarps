package games.indigo.playerwarps.bukkit.listeners;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import games.indigo.playerwarps.bukkit.Main;
import games.indigo.playerwarps.bukkit.events.PlayerWarpDeleteEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class PlayerWarpDeleteListener implements Listener {

    @EventHandler
    public void onPlayerWarpDelete(PlayerWarpDeleteEvent e) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("PlayerWarpsRefresh");

        e.getPlayer().sendPluginMessage(Main.getInstance(), "BungeeCord", out.toByteArray());
    }

}

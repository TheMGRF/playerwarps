package games.indigo.playerwarps.bukkit.listeners;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import games.indigo.playerwarps.bukkit.Main;
import games.indigo.playerwarps.bukkit.events.PlayerWarpCreateEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class PlayerWarpCreateListener implements Listener {

    private Main main = Main.getInstance();

    @EventHandler
    public void onPlayerWarpCreate(PlayerWarpCreateEvent e) {
        main.getWarpManager().saveWarp(e.getPlayerWarp());
        main.getWarpManager().getPlayerWarps().add(e.getPlayerWarp());

        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("PlayerWarpsRefresh");

        e.getPlayer().sendPluginMessage(Main.getInstance(), "BungeeCord", out.toByteArray());
    }

}

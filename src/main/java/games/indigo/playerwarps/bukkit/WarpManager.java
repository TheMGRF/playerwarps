package games.indigo.playerwarps.bukkit;

import games.indigo.playerwarps.core.PWLocation;
import games.indigo.playerwarps.core.PlayerWarp;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

public class WarpManager {

    private Main main = Main.getInstance();

    private final String DATABASE_NAME = "indigo";

    public ArrayList<PlayerWarp> playerWarps = new ArrayList<>();

    /**
     * Create the database table if it does not exist
     */
    public void createDatabase() {
        Bukkit.getScheduler().runTaskAsynchronously(main, () -> {
            try {
                Connection con = main.getDatabase().getConnection(DATABASE_NAME);

                PreparedStatement ps = con.prepareStatement(
                        "CREATE TABLE IF NOT EXISTS player_warps (" +
                                "`pwarp_uuid` INT(11) NOT NULL AUTO_INCREMENT," +
                                "`owner_uuid` VARCHAR(36) NOT NULL," +
                                "`name` VARCHAR(50) NOT NULL," +
                                "`description` TINYTEXT NOT NULL," +
                                "`material` VARCHAR(50) NOT NULL," +
                                "`world` VARCHAR(50) NOT NULL," +
                                "`x` DOUBLE NOT NULL," +
                                "`y` DOUBLE NOT NULL," +
                                "`z` DOUBLE NOT NULL," +
                                "`pitch` FLOAT NOT NULL," +
                                "`yaw` FLOAT NOT NULL," +
                                "`server` VARCHAR(50) NOT NULL," +
                                "PRIMARY KEY (`pwarp_uuid`)" +
                                ") COLLATE='latin1_swedish_ci' ENGINE=InnoDB;"
                );

                ps.execute();
                ps.close();
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public ArrayList<PlayerWarp> getPlayerWarps() {
        return playerWarps;
    }

    public void setPlayerWarps(ArrayList<PlayerWarp> playerWarps) {
        this.playerWarps = playerWarps;
    }

    public ArrayList<PlayerWarp> getPlayersWarps(String uuid) {
        ArrayList<PlayerWarp> playerWarps = new ArrayList<>();
        for (PlayerWarp playerWarp : getPlayerWarps()) {
            if (playerWarp.getUUID().equals(uuid)) {
                playerWarps.add(playerWarp);
            }
        }

        return playerWarps;
    }

    public int getAmountOfPlayerWarps(String uuid) {
        return getPlayersWarps(uuid).size();
    }

    /**
     * Add a player warp into the player warps array
     *
     * @param playerWarp The player warp to add
     */
    public void addPlayerWarp(PlayerWarp playerWarp) {
        playerWarps.add(playerWarp);
    }

    /**
     * Load player warps into memory from the database
     */
    public void loadWarps() {
        setPlayerWarps(new ArrayList<>());

        Bukkit.getScheduler().runTaskAsynchronously(main, () -> {
            try {
                Connection con = main.getDatabase().getConnection(DATABASE_NAME);

                PreparedStatement ps = con.prepareStatement("SELECT * FROM player_warps;");
                ResultSet rs = ps.executeQuery();

                while (rs.next()) {
                    String uuid = rs.getString("owner_uuid");
                    String name = rs.getString("name");
                    String description = rs.getString("description");
                    String material = rs.getString("material");
                    PWLocation location = new PWLocation(rs.getString("world"), rs.getDouble("x"), rs.getDouble("y"), rs.getDouble("z"), rs.getFloat("pitch"), rs.getFloat("yaw"));
                    String server = rs.getString("server");

                    playerWarps.add(new PlayerWarp(uuid, name, description, material, location, server));
                }

                if (!playerWarps.isEmpty()) Collections.shuffle(playerWarps); // Randomise the order of the player warps

                ps.execute();
                rs.close();
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Save a player warp to the database
     *
     * @param playerWarp The player warp to save
     */
    public void saveWarp(PlayerWarp playerWarp) {
        final String uuid = playerWarp.getUUID();
        final String name = playerWarp.getName();
        final String description = playerWarp.getDescription();
        final String material = playerWarp.getMaterial();
        final String world = playerWarp.getLocation().getWorld();
        final double x = playerWarp.getLocation().getX();
        final double y = playerWarp.getLocation().getY();
        final double z = playerWarp.getLocation().getZ();
        final float pitch = playerWarp.getLocation().getPitch();
        final float yaw = playerWarp.getLocation().getYaw();
        final String server = playerWarp.getServer();

        Bukkit.getScheduler().runTaskAsynchronously(main, () -> {
            try {
                Connection con = main.getDatabase().getConnection(DATABASE_NAME);

                PreparedStatement ps = con.prepareStatement("INSERT INTO player_warps (owner_uuid, name, description, material, world, x, y, z, pitch, yaw, server) VALUES ('" + uuid + "', '" + name + "', '" + description + "', '" + material + "', '" + world + "', " + x + ", " + y + ", " + z + ", " + pitch + ", " + yaw + ", '" + server + "');");
                ps.execute();
                ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Save all player warps to the database
     */
    public void saveAllWarps() {
        getPlayerWarps().forEach(this::saveWarp);
    }

    /**
     * Remove a player warp
     *
     * @param playerWarp The player warp to remove
     */
    public void removePlayerWarp(PlayerWarp playerWarp) {
        final String uuid = playerWarp.getUUID();
        final String name = playerWarp.getName();

        getPlayerWarps().remove(playerWarp);

        Bukkit.getScheduler().runTaskAsynchronously(main, () -> {
            try {
                Connection con = main.getDatabase().getConnection(DATABASE_NAME);

                PreparedStatement ps = con.prepareStatement("DELETE FROM player_warps WHERE owner_uuid = '" + uuid + "' AND name = '" + name + "';");
                ps.execute();
                ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public PWLocation asPWLocation(Location location) {
        return new PWLocation(location.getWorld().getName(), location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());
    }

    public Location asBukkitLocation(PWLocation location) {
        return new Location(Bukkit.getWorld(location.getWorld()), location.getX(), location.getY(), location.getZ(), location.getPitch(), location.getYaw());
    }
}

package games.indigo.playerwarps.bukkit.commands;

import games.indigo.playerwarps.bukkit.Main;
import games.indigo.playerwarps.core.PlayerWarp;
import games.indigo.playerwarps.bukkit.guis.PlayerWarpsGUI;
import games.indigo.playerwarps.bukkit.utils.EconomyHook;
import games.indigo.playerwarps.bukkit.utils.Text;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

public class PlayerWarpsCommand implements CommandExecutor {

    private Main main = Main.getInstance();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (args.length > 0) {
                if (args[0].equalsIgnoreCase("help")) {
                    help(player);
                } else if (args[0].equalsIgnoreCase("create")) {
                    // Check if player has enough money
                    if (EconomyHook.getEconomy().has(player, EconomyHook.getCost())) {
                        if (player.getInventory().getItemInMainHand().getType() != Material.AIR) {
                            player.sendMessage(Text.colour(Text.PREFIX + "&aWarp icon set to &f" + player.getInventory().getItemInMainHand().getType()));
                            player.sendMessage(Text.colour(Text.PREFIX + "&7Please enter a name for your warp &c(to exit this type \"cancel\")&7:"));
                            player.setMetadata("pWarpsName", new FixedMetadataValue(main, true));
                            player.setMetadata("pWarpsCurrent", new FixedMetadataValue(main, new PlayerWarp(player.getUniqueId().toString(), "", "", player.getInventory().getItemInMainHand().getType().toString(), main.getWarpManager().asPWLocation(player.getLocation()), StringUtils.capitalize(player.getWorld().getName()))));
                        } else {
                            player.sendMessage(Text.colour(Text.PREFIX + "&cPlease hold the item you wish to use as an icon for your warp then try the command again."));
                        }
                    } else {
                        player.sendMessage(Text.colour(Text.PREFIX + "&cYou do not have enough money for a warp. &a" + EconomyHook.getEconomy().format(EconomyHook.getEconomy().getBalance(player)) + "&7/&c" + EconomyHook.getEconomy().format(EconomyHook.getCost())));
                    }
                } else if (args[0].equalsIgnoreCase("list")) {
                    PlayerWarpsGUI.open(player, 1);
                } else if (args[0].equalsIgnoreCase("reload")) {
                    if (player.hasPermission("indigo.pwarps.reload")) {
                        main.getWarpManager().loadWarps();
                        player.sendMessage(Text.colour(Text.PREFIX + "&aSuccessfully reloaded."));
                    } else {
                        player.sendMessage(Text.NO_PERMS);
                    }
                } else {
                    help(player);
                }
            } else {
                PlayerWarpsGUI.open(player, 1);
            }
        } else {
            sender.sendMessage("no");
        }
        return true;
    }

    private void help(Player player) {
        player.sendMessage(Text.colour("&b----- &3&lPlayer Warps &b-----"));
        player.sendMessage("");
        player.sendMessage(Text.colour("&3/pwarp help &7- &fView this help menu"));
        player.sendMessage(Text.colour("&3/pwarp create &7- &fCreate a player warp"));
        player.sendMessage(Text.colour("&3/pwarp list &7- &fOpen the player warps menu"));
    }
}

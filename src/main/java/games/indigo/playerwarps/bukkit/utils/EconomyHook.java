package games.indigo.playerwarps.bukkit.utils;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;

public class EconomyHook {

    private static Economy econ;

    public static Economy getEconomy() { return econ; }

    public static int getCost() { return 100000; }

    public static void setupEconomy() {
        Bukkit.getLogger().info("Loading vault!");
        if (Bukkit.getPluginManager().getPlugin("Vault") != null) {
            Bukkit.getLogger().info("Loaded vault!");

            RegisteredServiceProvider<Economy> rsp = Bukkit.getServer().getServicesManager().getRegistration(Economy.class);
            if (rsp == null) {
                return;
            }
            econ = rsp.getProvider();
        } else {
            Bukkit.getLogger().severe("Loading Vault failed! Vault is null!");
            Bukkit.getLogger().info("Plugin will now operate without Vault support...");
        }
    }

}

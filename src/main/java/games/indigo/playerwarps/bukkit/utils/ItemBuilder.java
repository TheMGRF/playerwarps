package games.indigo.playerwarps.bukkit.utils;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ItemBuilder {

    /**
     * Build an item stack
     * @param material The material to use in the item stack
     * @param amount How many items should be in the item stack
     * @param name The display name of the item stack
     * @return The finished item stack
     */
    public static ItemStack buildItem(Material material, int amount, String name) {
        ItemStack item = new ItemStack(material, amount);
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(Text.colour(name));

        item.setItemMeta(itemMeta);

        return item;
    }

    /**
     * Build an item stack
     * @param material The material to use in the item stack
     * @param amount How many items should be in the item stack
     * @param name The display name of the item stack
     * @param description The lore of the item stack
     * @return The finished item stack
     */
    public static ItemStack buildItem(Material material, int amount, String name, List<String> description) {
        ItemStack item = new ItemStack(material, amount);
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(Text.colour(name));
        itemMeta.setLore(Text.colourArray(description));

        item.setItemMeta(itemMeta);

        return item;
    }

    public static ItemStack warpBuilder(String name, String description, String material, String owner, Location location, String server) {
        ArrayList<String> lore = new ArrayList<>();
        for (String descLine : description.split("(?<=\\G.{42,}\\s)")) {
            lore.add("&7" + ChatColor.translateAlternateColorCodes('&', descLine));
        }
        lore.addAll(Arrays.asList(
                "",
                "&7Realm: &e" + server,
                "&7Location: &e" + location.getBlockX() + ", " + location.getBlockY() + ", " + location.getBlockZ(),
                "",
                "&7Owner: &e" + owner,
                "",
                "&6Click to teleport!"
        ));

        ItemStack item = buildItem(Material.valueOf(material), 1, name, lore);
        item.getItemMeta().addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        return item;
    }

    public static ItemStack ownWarpBuilder(String name, String description, String material, Location location, String server) {
        ArrayList<String> lore = new ArrayList<>();
        for (String descLine : description.split("(?<=\\G.{42,}\\s)")) {
            lore.add("&7" + ChatColor.translateAlternateColorCodes('&', descLine));
        }
        lore.addAll(Arrays.asList(
                "",
                "&7Realm: &e" + server,
                "&7Location: &e" + location.getBlockX() + ", " + location.getBlockY() + ", " + location.getBlockZ(),
                "",
                "&fLeft-Click: &6Teleport to warp",
                "&fShift + Left-Click: &6Change warp name & description",
                "&fShift + Right-Click: &6Update warp location",
                "&fMiddle click: &6Update icon",
                "&fRight-Click: &4Delete"
        ));

        ItemStack item = buildItem(Material.valueOf(material), 1, name, lore);
        item.getItemMeta().addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        return item;
    }
}

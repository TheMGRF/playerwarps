package games.indigo.playerwarps.bukkit.guis;

import games.indigo.playerwarps.bukkit.utils.ItemBuilder;
import games.indigo.playerwarps.bukkit.utils.Text;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class ConfirmDeleteGUI {

    public static void open(Player player) {
        Inventory inv = Bukkit.createInventory(null, 9, Text.colour("&4Delete Warp &8| &aAre you sure?"));

        inv.setItem(3, ItemBuilder.buildItem(Material.LIME_WOOL, 1, "&a&lCONFIRM"));
        inv.setItem(5, ItemBuilder.buildItem(Material.RED_WOOL, 1, "&c&lCANCEL"));

        player.openInventory(inv);
    }

}

package games.indigo.playerwarps.bukkit.guis;

import games.indigo.playerwarps.bukkit.Main;
import games.indigo.playerwarps.bukkit.utils.ItemBuilder;
import games.indigo.playerwarps.bukkit.utils.Text;
import games.indigo.playerwarps.core.PlayerWarp;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.Arrays;
import java.util.UUID;

public class PlayerWarpsGUI {

    private static Main main = Main.getInstance();

    public static void open(Player player, int page) {
        Inventory inv = Bukkit.createInventory(null, 54, Text.colour("&b&lPlayer Warps &8(Page " + page + ")"));

        int slot = 0;
        int itemIndex = 45 * (page - 1);

        int loop = 0;
        for (PlayerWarp playerWarp : main.getWarpManager().getPlayerWarps()) {
            if (loop >= itemIndex && slot < 45) {

                inv.setItem(slot, ItemBuilder.warpBuilder(playerWarp.getName(), playerWarp.getDescription(), playerWarp.getMaterial(), Bukkit.getOfflinePlayer(UUID.fromString(playerWarp.getUUID())).getName(), main.getWarpManager().asBukkitLocation(playerWarp.getLocation()), playerWarp.getServer()));
                slot++;
            }
            loop++;
        }

        // Footer
        setFooter(inv);

        player.setMetadata("pwarpsPage", new FixedMetadataValue(main, page));

        player.openInventory(inv);
    }

    private static void setFooter(Inventory inv) {
        inv.setItem(45, ItemBuilder.buildItem(Material.OAK_SIGN, 1, "&6View Own Warps"));

        inv.setItem(48, ItemBuilder.buildItem(Material.MAP, 1, "&bPrevious Page"));

        inv.setItem(49, ItemBuilder.buildItem(Material.SUNFLOWER, 1, "&eRefresh"));

        inv.setItem(50, ItemBuilder.buildItem(Material.MAP, 1, "&bNext Page"));

        inv.setItem(53, ItemBuilder.buildItem(Material.BARRIER, 1, "&cExit"));

        inv.setItem(53, ItemBuilder.buildItem(Material.KNOWLEDGE_BOOK, 1, "&2What is this?", Arrays.asList(
                "&7This is the &3Player Warps &7menu where",
                "&7you can teleport to warps that other",
                "&7players have created. You can get",
                "&7your own warps by using &b/pwarps create"
        )));
    }

}

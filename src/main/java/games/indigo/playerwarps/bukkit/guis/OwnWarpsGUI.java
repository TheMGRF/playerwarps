package games.indigo.playerwarps.bukkit.guis;

import games.indigo.playerwarps.bukkit.Main;
import games.indigo.playerwarps.bukkit.utils.ItemBuilder;
import games.indigo.playerwarps.bukkit.utils.Text;
import games.indigo.playerwarps.core.PlayerWarp;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.Arrays;

public class OwnWarpsGUI {

    private static Main main = Main.getInstance();

    public static void open(Player player) {
        Inventory inv = Bukkit.createInventory(null, 54, Text.colour("&b&lPlayer Warps &8(" + player.getName() + ")"));

        int slot = 0;

        for (PlayerWarp playerWarp : main.getWarpManager().getPlayersWarps(player.getUniqueId().toString())) {
            if (slot <= 44) {
                inv.setItem(slot, ItemBuilder.ownWarpBuilder(playerWarp.getName(), playerWarp.getDescription(), playerWarp.getMaterial(), main.getWarpManager().asBukkitLocation(playerWarp.getLocation()), playerWarp.getServer()));
                slot++;
            }
        }

        // Footer
        setFooter(inv);

        player.openInventory(inv);
    }

    private static void setFooter(Inventory inv) {
        inv.setItem(45, ItemBuilder.buildItem(Material.OAK_SIGN, 1, "&6View All Warps"));

        inv.setItem(49, ItemBuilder.buildItem(Material.NETHER_STAR, 1, "&dCreate Warp", Arrays.asList(
                "&3Player Warp&7: &a$100,000",
                "",
                "&7You will be prompted to type the name and",
                "&7description of your new warp in chat!",
                "&7A warp will use the current item in your",
                "&7main hand as its icon!",
                "",
                "&bClick here to create a warp!"
        )));

        inv.setItem(53, ItemBuilder.buildItem(Material.KNOWLEDGE_BOOK, 1, "&2What is this?", Arrays.asList(
                "&7This is your &3Player Warps &7control panel.",
                "&7Here you can edit and create your warps!",
                "",
                "&7Hover over each warp to see its options or",
                "&7click the &dNether Star &7to create a warp!",
                "",
                "&4&lWARNING",
                " &7Deleting a warp will &cnot &7refund any money!",
                " &7Staff will not give back any money if you",
                " &7delete a warp, even by accident!"
        )));
    }

}

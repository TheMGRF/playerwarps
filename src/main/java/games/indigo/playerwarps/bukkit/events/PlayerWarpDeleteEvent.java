package games.indigo.playerwarps.bukkit.events;

import games.indigo.playerwarps.core.PlayerWarp;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerWarpDeleteEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private PlayerWarp playerWarp;
    private Player player;

    public PlayerWarpDeleteEvent(PlayerWarp playerWarp, Player player) {
        this.playerWarp = playerWarp;
        this.player = player;
    }

    public PlayerWarp getPlayerWarp() {
        return playerWarp;
    }

    public Player getPlayer() {
        return player;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}

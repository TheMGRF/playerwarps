package games.indigo.playerwarps.core;

public class PlayerWarp {

    private String uuid, name, description, material, server;
    private PWLocation location;

    /**
     * @param uuid The pwarp's player's UUID
     * @param name The name the user gave the pwarp
     * @param description The description the user gave the pwarp
     * @param material The material value the user assigned to the pwarp
     */
    public PlayerWarp(String uuid, String name, String description, String material, PWLocation location, String server) {
        this.uuid = uuid;
        this.name = name;
        this.description = description;
        this.material = material.toUpperCase();
        this.location = location;
        this.server = server;
    }

    /**
     * Empty constructor for using fromString
     */
    public PlayerWarp() {

    }

    /**
     * Get the uuid of the users pwarp
     * @return uuid The uuid of the users pwarp
     */
    public String getUUID() {
        return uuid;
    }

    /**
     * Set the uuid of the users pwarp
     * @param uuid The uuid of the users pwarp
     */
    public void setUUID(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Get the name of the users pwarp
     * @return name The name of the users pwarp
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name of the users pwarp
     * @param name The name of the users pwarp
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the descrition of the users pwarp
     * @return description The description of the users pwarp
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the description of the users pwarp
     * @param description The description of the users pwarp
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Get the material value of the users pwarp
     * @return material The material value of the uers pwarp
     */
    public String getMaterial() {
        return material;
    }

    /**
     * Set the material value of the users pwarp
     * @param material The material value of the users pwarp
     */
    public void setMaterial(String material) {
        this.material = material.toUpperCase();
    }

    /**
     * Get the location of the users pwarp
     * @return location The location of the users pwarp
     */
    public PWLocation getLocation() {
        return location;
    }

    /**
     * Set the location of the users pwarp
     * @param location The location of the users pwarp
     */
    public void setLocation(PWLocation location) {
        this.location = location;
    }

    /**
     * Get the server the player warp is on
     * @return The server the player warp is on
     */
    public String getServer() {
        return server;
    }

    /**
     * Set the server the player warp is on
     * @param server The server the player warp is on
     */
    public void setServer(String server) {
        this.server = server;
    }

    private String splitter = "@;";

    /**
     * Convert the object to a string
     * @return A string formatted version of the object
     */
    @Override
    public String toString() {
        return uuid + splitter + name + splitter + description + splitter + material + splitter + location.getWorld() + splitter + location.getX() + splitter + location.getY() + splitter + location.getZ() + splitter + location.getPitch() + splitter + location.getYaw() + splitter + server;
    }

    /**
     * Create a player warp from a string
     * @return The player warp
     */
    public PlayerWarp fromString(String string) {
        String[] pw = string.split(splitter);

        return new PlayerWarp(pw[0], pw[1], pw[2], pw[3], new PWLocation(pw[4], Double.parseDouble(pw[5]), Double.parseDouble(pw[6]), Double.parseDouble(pw[7]), Float.parseFloat(pw[8]), Float.parseFloat(pw[9])), pw[10]);
    }
}

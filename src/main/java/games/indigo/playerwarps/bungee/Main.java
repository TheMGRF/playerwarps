package games.indigo.playerwarps.bungee;

import net.md_5.bungee.api.plugin.Plugin;

public class Main extends Plugin {

    private static Main instance;

    @Override
    public void onEnable() {
        instance = this;

        getProxy().getPluginManager().registerListener(this, new PluginMessageReceiver());

        getProxy().registerChannel("BungeeCord");
    }

    public static Main getInstance() {
        return instance;
    }
}

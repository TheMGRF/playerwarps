package games.indigo.playerwarps.bungee;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import games.indigo.playerwarps.core.PlayerWarp;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class PluginMessageReceiver implements Listener {

    @EventHandler
    public void onMessage(PluginMessageEvent e) {
        if (e.getTag().equals("BungeeCord")) {
            ByteArrayDataInput in = ByteStreams.newDataInput(e.getData());
            String subChannel = in.readUTF();

            if (subChannel.equalsIgnoreCase("PlayerWarps")) {
                ProxiedPlayer player = ProxyServer.getInstance().getPlayer(UUID.fromString(in.readUTF()));

                PlayerWarp playerWarp = new PlayerWarp().fromString(in.readUTF());

                ServerInfo targetServer = ProxyServer.getInstance().getServerInfo(playerWarp.getServer().toLowerCase());
                if (player.getServer().getInfo() != targetServer) {
                    player.connect(targetServer);
                }

                ByteArrayDataOutput out = ByteStreams.newDataOutput();
                out.writeUTF("PlayerWarps" + UUID.randomUUID().toString());
                out.writeUTF(player.getUniqueId().toString());
                out.writeUTF(playerWarp.toString());

                // Send the player warp information to the target server
                ProxyServer.getInstance().getScheduler().schedule(Main.getInstance(), new Runnable() {
                    @Override
                    public void run() {
                        targetServer.sendData("BungeeCord", out.toByteArray());
                    }
                }, 20, TimeUnit.MILLISECONDS);
            } else if (subChannel.equalsIgnoreCase("PlayerWarpsRefresh")) {
                ByteArrayDataOutput out = ByteStreams.newDataOutput();
                out.writeUTF("PlayerWarpsRefresh");

                ProxyServer.getInstance().getServers().forEach((s, serverInfo) -> serverInfo.sendData("BungeeCord", out.toByteArray()));
            }
        }
    }
}
